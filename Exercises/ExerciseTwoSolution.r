
## Exercise 2
# Read in data
data <- read.csv("PD_Data_R_Training.csv")
# Calculate length of stay
RefDate <- as.Date(data$ReferralDate, "%d/%m/%Y")       #Calculate length of stay
RefDischarge <- as.Date(data$ReferralDischarge, "%d/%m/%Y")
los <- as.numeric(RefDischarge - RefDate)
data$los <- los
# Plot length of stay as a histogram
hist(data$los)
# Describe length of stay
library(psych)
losDes <- describe(data$los)
losClusDes <- describeBy(data$los, group=data$Cluster)
losClusDes[1]
losClusDes[2]
losClusDes[[1]][1]
losClusDes[[2]][4]
# Subset by cluster plot los as hist, conduct t-test on los and plot as boxplot
clusSeven <- subset(data, data$Cluster == 7)
clusEight <- subset(data, data$Cluster == 8)
clusNA <- subset(data, is.na(data$Cluster))
hist(clusSeven$los)
hist(clusEight$los)
hist(clusNA$los)
t.test(clusSeven$los,clusEight$los)
t.test(clusSeven$los,clusNA$los)
t.test(clusEight$los,clusNA$los)
boxplot(clusSeven$los,clusEight$los,clusNA$los, outline=FALSE)
