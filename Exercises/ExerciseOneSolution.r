
## Exercise 1
# Set the working directory if necessary
# setwd("~/Documents/R working folder/R training")
# Read in the data set
data <- read.csv("PD_Data_R_Training.csv")
# Calculate length of stay
RefDate <- as.Date(data$ReferralDate, "%d/%m/%Y")       #Calculate length of stay
RefDischarge <- as.Date(data$ReferralDischarge, "%d/%m/%Y")
los <- as.numeric(RefDischarge - RefDate)
data$los <- los
# Subset data by setting
a <- row.names(table(data$Setting))
commData <- data[ which(data$Setting == "Community"),]
inpatData <- data[ which(data$Setting == "Inpatient"),]
ooaData <- data[ which(data$Setting == "OOA"),]
otherData <- data[ which(data$Setting == "Other local beds"),]
# Table ward team and ICD-10 codes for each subset
commTeamTab <- table(commData$WardTeam)
commCodeTab <- table(commData$ICD10)
# Table ICD-10 codes by cluster
commCodeClusTab <- table(commData$ICD10,commData$Cluster)
# For loop to get total and mean los for each patient
patLos <- data[c("ClientID","los")]
patLos <- na.omit(patLos)
uniId <- unique(patLos$ClientID)
totLos <- rep(0,length(uniId))
meanLos <- rep(0,length(uniId))
for(i in 1:length(uniId)){
  a <- subset(patLos, patLos$ClientID == uniId[i])
  totLos[i] <- sum(a$los)
  meanLos[i] <- mean(a$los)
}
patSumMat <- cbind(uniId,totLos,meanLos)
patSumMat[1:10,]
