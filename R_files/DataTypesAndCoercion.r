
x <- c(1,2,3,4)
class(x)
y <- c('red','green','blue','yellow')
class(y)

data <- seq(1,50,2)
y <- matrix(data,5,5,byrow=TRUE)
str(y)
data2 <- data.frame(ID=seq(1,25,1),
                   ScoreOne=rpois(25,20),
                   ScoreTwo=rpois(25,35))
str(data2)

x <- c(1,2,3,4)
class(x)
x <- as.character(x)
class(x)
y <- c('red','green','blue','yellow')
class(y)
y <- as.factor(y)
class(y)
str(y)
z <- c("TRUE","FALSE","TRUE","TRUE","FALSE","FALSE")
class(z)
z <- as.logical(z)
class(z)
