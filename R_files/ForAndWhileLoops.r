
x <- 1:10
x
y <- rep(0,length(x))
for (i in 1:length(x)){
    y[i] <- x[i] + i
}
y

nums <- sample(1:50,20,replace=T)
x <- matrix(nums,nrow=4,ncol=5)
x
out <- matrix(rep(0,20),nrow=4,ncol=5)
for (i in 1:nrow(x)){
    for (j in 1:ncol(x)){
        out[i,j] <- x[i,j] + i
    }
}
out

x <- 1:10
x
y <- rep(0,10)
i <- 1
while (i < 6){
  y[i] <- x[i] + i
  i <- i+1
}

print(y)
