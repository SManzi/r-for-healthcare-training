
set.seed(1)
x <- rnorm(1000, 50, 2)
y <- rnorm(1000, 25, 2)
z <- as.factor(sample(1:2,1000,replace=TRUE))
plot(x,y,xlab="Outcome measure 1",
     ylab="Outcome measure 2",
     main="Comparison of outcome measures",col=z)

x <- c(0,5,10,15,20,25,30)
y <- c(81,95,102,120,110,108,92)
z <- c(78,90,100,118,125,112,96)
plot(c(x,x),c(y,z),xlab="Time",ylab="Heart rate",
     main="Patient heart rate during exercise",pch=c(1,2))
lines(x,y,col="red",lty=1)
lines(x,z,col="blue",lty=2)
legend("topleft",c("Patient 1","Patient 2"),
       col=c("red","blue"),lty=c(1,1))
