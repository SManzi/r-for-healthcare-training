
a <- c(2,8,4)
b <- c(4,9,2)
c <- c(3,5,7,3,2,6)
e <- a > b
f <- b != a
g <- a <= c
h <- c <= a

x <- 3
y <- 5
if (x < y){
    z <- x - y
}else{
    z <- x + y
}
print(z)

if (y > x && x > z){
    print('z is lowest and y is highest')
}else if (x > y && y > z ){
    print('x is highest and z is lowest')
}else if (z > x && x > y){
    print('z is highest and y is lowest')
}else{
    print('try another combination of comparisons')
}

if (y <= x || y >= z){
    print(y)
}

a <- 1:6
print(a)
(a > 1) & (a <= 5)
