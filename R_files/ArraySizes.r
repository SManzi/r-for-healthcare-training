
V<-c(1:41)
length(V)

M<-matrix(1:9,nrow=3)
M

nrow(M)
ncol(M)

dim(M)

M2<-2*M
M3<-3*M
N<-array(c(M,M2,M3),c(3,3,3))#3dimensional stacking of the three matrices using the concatenation operator c()
dim(N)
