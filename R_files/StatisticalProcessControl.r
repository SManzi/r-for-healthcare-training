
library(qicharts)
set.seed(10)

y <- rpois(24, 10)
a <- qic(y, runvals=TRUE)

y[13:24] <- rpois(12, 5)
b <- qic(y, runvals=TRUE)

qic(y,freeze=12)

qic(y,breaks=12)

y <- rbinom(24, 30, 0.2)
n <- sample(22:30,24,replace=TRUE)
qic(y,n)

week <- seq(as.Date("2016-01-01"),length.out=24,by="week")
notes <- NA
notes[17] <- "Note for this point"
qic(y, n, x=week, main="Run chart", ylab="Proportion",
    xlab="Week", notes=notes)

u.referrals <- 30
u.r2a <- 1
u.days <- u.referrals*u.r2a
p.Miss <- 0.2

referrals <- rpois(24,lambda=u.referrals)
patientdays <- round(rnorm(24,mean=u.days,sd=4))
n.Miss <- rpois(24, lambda=u.referrals*p.Miss)
n.pat.Miss <- rbinom(24,size=u.referrals,prob=p.Miss)
week <- seq(as.Date("2016-01-01"),length.out=24,by="week")

d <- data.frame(week,referrals,patientdays,n.Miss,n.pat.Miss)
d

c <- qic(n.Miss, x=week, data=d, chart="c",
         ylab="count", xlab="week", runvals=TRUE)
c

e <- qic(n.Miss, n=patientdays, x=week, data=d,
         chart="u", multiply=1, ylab="count", xlab="week")
e

f <- qic(n.Miss, n=referrals, x=week, data=d,
         chart="p", multiply=100, ylab="count", xlab="week")
f
