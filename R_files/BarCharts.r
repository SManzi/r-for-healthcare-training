
x <- c(rep("Cluster 4",24), rep("Cluster 5", 31),
       rep("Cluster 6",14))
xTab <- table(x)
xTab
barplot(xTab, xlab="Care cluster", ylab="Frequency",
        main="Occurrences of care clusters")

x <- c(rep("Cluster 4",24), rep("Cluster 5", 31),
       rep("Cluster 6",14),rep("Cluster 4",17),
       rep("Cluster 5", 23), rep("Cluster 6",29))
y <- c(rep("2016",69),rep("2017",69))
xyTab <- table(x,y)
xyTab
barplot(xyTab, xlab="Care cluster", ylab="Frequency",
        main="Occurrences of care clusters",
        col=c("red","green","blue"), beside=TRUE,
        legend.text=TRUE, args.legend=list(x=5.5,y=30))
