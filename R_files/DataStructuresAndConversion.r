
a<-1:5
a

b<-c("Fish","Cat","Dog")
b

a[6]<-6
a

a[4]<-11
a

x<-array(1:20,dim=c(4,5)) 
x

x[1,5]

x[,1]
x[4,]

# Creates a 2 x 3 matrix populated with 7's
a <- matrix(7, 4, 5)
a
# Creates a 2 x 3 matrix populated with 9's.
b <- matrix(9, 4, 5)
b
# Creates a 4 x 5 x 2 array with two levels,
# one containing 7's and the other containing 9's
c<-array(c(a, b), c(4, 5,2))

c[1,4,1]<-4 # the entry of row 1, column 4, matrix 1
c[,3,1]<-1  # the entire column 3
c[2,,1]<-2 # row 2 of matrix 1
c[,,1] # print the whole matrix 1
c[,,2] # print the whole matrix 2

data<-as.data.frame(matrix(1:4,nrow=2))
data

a<-matrix(1:6,nrow =2)
names<-c("ID","height","weight")
data<-as.data.frame(a)
colnames(data)<-names
data

matData <- as.matrix(data)
matData
