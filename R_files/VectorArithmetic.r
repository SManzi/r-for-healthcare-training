
a<-matrix(1:4,nrow=2,ncol=2)
b<-matrix(seq(5,8),nrow=2,ncol=2)
a 
b

a+b
a-b

c<-matrix(1:6,nrow=3)
d<-c(2,2)
d*c
c*d

c<-matrix(1:4,nrow =2,ncol=2)
d<-matrix(5,9,nrow=2,ncol =2)
c*d
d*c

a%*%b

b%*%a

a<-matrix (c(2,3,4,5,6,7,8,9),nrow=2,ncol =4)
a
b<-matrix(c(11,12,13,14,15,16,1,2,3,21,22,23),nrow=4,ncol=3)
b

try(a%*%b)
try(b%*%a)
