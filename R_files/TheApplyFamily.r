
x <- matrix(rnorm(25),nrow=5,ncol=5)
x
y <- apply(x,2,mean)
y

a <- matrix(rnorm(25),nrow=5,ncol=5)
b <- matrix(rnorm(25),nrow=5,ncol=5)
c <- matrix(rnorm(25),nrow=5,ncol=5)
matList <- list(a,b,c)
matList
y <- lapply(matList,"[",,2)
y
y[1]

y <- sapply(matList,"[",1,2)
y
z <- sapply(matList, function(x) x+5)
z

z <- vapply(y, function(x) x+5, numeric(1))
z

a <- matrix(rnorm(25),nrow=5,ncol=5)
a
x <- mapply(function(x) x*2, a)
x

l <- list(1,list(2,3),4,5,list(6,7,8),9)
l
x <- rapply(l,function(x) (x*2)^3)
x
y <- rapply(l,function(x) (x*2)^3, how="list")
y

a <- 1:5
b <- c("F","M","M","F","F")
b <- as.factor(b)
c <- c(34,26,42,47,22)
d <- cbind(a,b,c)
data <- as.data.frame(d)
data

x <- tapply(data$c,b,sum,simplify=FALSE)
x
