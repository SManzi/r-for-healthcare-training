
A<-matrix(c(1,2,3,4),nrow =2, ncol =2)
A

#Now try adding another row
B<-c(5,6)
C<-rbind(A,B)
C

# Use cbind to add a column instead
D<-c(5,6)
E<-cbind(A,D)
E

a<-c(1,2,3,4)
b<-append(a,c(5,6))
b

a<-c(1,2,3,4)
b<-append(a,c(5,6), after =2)
b

clinical_trial1 <-
    data.frame(patient = 1:100,
               age = signif(rnorm(100, mean = 60, sd = 6),
               digits=2),treatment = gl(2, 50,
               labels = c("Treatment", "Control")),
               centre = sample(paste("Centre", LETTERS[1:5]),
                  100,replace = TRUE))

clinical_trial2 <-
    data.frame(patient = 1:100,
               BMI = rpois(100, lambda=25),
               gender = rep(gl(2, 25,
               labels = c("Male", "Female"),ordered=FALSE),
                  times=2))

clinical_trial3<-merge(clinical_trial1, clinical_trial2,
                       by.x="patient", by.y="patient",
                       all=TRUE) 
clinical_trial3[1:10,]

GP<-rep(1:5,20) #i.e 100 observations.

clinical_trial4<-cbind(clinical_trial3,GP)
clinical_trial4[1:10,]

row<-c(101,57,"Treatment","Centre D", 27,
      "Male", 4) #one row of 7 columns.
clinical_trial5<-rbind(clinical_trial4,row)
clinical_trial5[91:101,]
