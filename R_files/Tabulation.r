
clinical_trial <-
    data.frame(patient = 1:100,
               age = rnorm(100, mean = 60, sd = 6),
               treatment = gl(2, 50,
                 labels = c("Treatment", "Control")),
               centre = sample(paste("Centre", LETTERS[1:5]),
                 100, replace = TRUE))

attach(clinical_trial)

clinical_trial[1:10, ]

table(treatment)

table(treatment, centre, dnn=c("TREATMENTS", "CENTRES"))

centres<-as.data.frame(table(treatment, centre),
                       responseName="counts")

centres
