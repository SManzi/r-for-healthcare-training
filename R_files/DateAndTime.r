
df_birth<-data.frame(ID=1:10,dob=c("12/04/1976","16/06/1965","23/11/1985",
                      "24/02/1973","01/04/1946","27/05/1983","09/08/2001",
                      "30/03/1957","14/07/2007","19/12/1994"))
df_birth

attach(df_birth)

dob[1] -"23/3/45"

dob_int<-as.Date(dob, "%d/%m/%Y") #4 digit year
df_birth1<-data.frame(dob_int,df_birth)
df_birth1

str(df_birth1)

day<-weekdays(df_birth1$dob_int)
num_days<-julian(df_birth1$dob_int)
day
num_days

time<-as.POSIXct(c("12/09/1971 23:49:00","09/06/1970 04:07:57"),
                 format="%d/%m/%Y %H:%M:%S")
time
time[2]-time[1]

x <- c("06-07-19, 5:12am", "06-07-20, 5:15am",
       "06-07-21, 5:18pm", "06-07-22, 5:22am",
       "06-07-23, 5:25am")
dct <- as.POSIXct(x,format="%y-%m-%d, %I:%M%p")
#%I = 12 hour clock, %p = am/pm format
dlt <- strptime(x, format="%y-%m-%d, %I:%M%p")

dct #this is now stored as the number of seconds since 1/1/1970.
#To see this type:
unclass(dct)
dlt

dlt[4]-dlt[1]#  differences
dlt[4]$min-dlt[1]$min# DIFFERENCE BETWEEN THE MINUTE COMPONENTS
dct[4]-dct[1]
