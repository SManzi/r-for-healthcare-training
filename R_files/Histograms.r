
x <- rexp(1000, 10)
hist(x,xlab="Inter-arrival time (Time unit = hours)",
     ylab="Frequency",
     main="Inter-arrival time of people to emergency department")

hist(x,xlab="Inter-arrival time (Time unit = hours)",
     ylab="Frequency",
     main="Inter-arrival time of people to emergency department",
     breaks=15,xlim=c(0,1),ylim=c(0,500))

h <- hist(x,xlab="Inter-arrival time (Time unit = hours)",
          ylab="Frequency",
          main="Inter-arrival time of people to emergency department",
          breaks=15)
h
maxH <- max(h$counts)
yMax <- (maxH + ((maxH/100)*20))
hist(x,xlab="Inter-arrival time (Time unit = hours)",ylab="Frequency",
     main="Inter-arrival time of people to emergency department",
     breaks=15,xlim=c(0,1),ylim=c(0,yMax))
